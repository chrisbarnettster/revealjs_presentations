
# Presentations with Reveal

 * Presentations made using the reveal js framework

* TLDR I've always enjoyed the idea of writing presentation in markdown. I've used sphinx (https://bitbucket.org/rxncor/intranet), sphinx with hieroglyph (requires installing stuff https://bitbucket.org/rxncor/hieroglyph_slides/overview) and even reveal.js a few haphazard times.
Without relying on a slideshare service I just want to use something quick like reveal and for it to work... but time and time again.
[This article](https://opensource.com/education/13/10/teaching-with-revealjs) discusses short *vs* long term strategies for using reveal.
Let's invest.


# Getting started
## Making a new presentation
1. clone this repo and the submodules
`git clone --recursive  git@bitbucket.org:chrisbarnettster/revealjs_presentations.git`
2. make a branch and checkout a branch
`git checkout -b cb/pres/viswall2016`

3. hack index.html
use favourite editor and hack. Check it looks good by loading in the browser.

4. add and commit changes
```
git status # should be only a few changes maybe index.html and images
git add media/images/ index.html
git commit -m "a useful commit message for a new presentation"
```

5. repeat updates as required

6. **Never merge branch back into master.** Choose to change master if the template index.html needs changing or the submodule does.Otherwise stay in branches.
Alternatively clone and do your own thing.
 - What if you want to take beneficial master branch changes and merge these into a branch?
    * checkout your existing branch
      `git checkout yourbranch`
    * merge but never commit even if no conflicts
      `git merge --no-commit master`
    * diff and check content, open presentation etc.
      `git difftool --cached`
    * ARRGGH, need to back out? `git reset --merge` else...
    * `git commit -m "merge update from master branch"`




## Too complicated?
- Look at demo.html for ideas.
- Look at https://github.com/hakimel/reveal.js/wiki/Example-Presentations for inspiration
- Still too hard? Try http://slides.com

## Development and Documentation
More details and docs at https://github.com/hakimel/reveal.js/


### Plugins
Resources
- https://github.com/rajgoel/reveal.js-plugins
- http://courses.telematique.eu/reveal.js-plugins/anything-demo.html#/

#### Additional Plugins installed
- Menu plugin
  - `git submodule add https://github.com/denehyg/reveal.js-menu plugins/menu`
  - Add to index.html `{ src: 'plugins/menu/menu.js' }`

## how this was created
As per [this article](https://opensource.com/education/13/10/teaching-with-revealjs)

- `git init .`
- `git submodule add https://github.com/hakimel/reveal.js revealjs`
- copied example index.html and demo.html from revealjs to top level
- prepend revealjs to  css, plugin links
